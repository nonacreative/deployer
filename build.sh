#!/usr/bin/env bash

set -e

# Colours
red='\e[0;31m'
yellow='\e[0;33m'
blue='\e[0;34m'
cyan='\e[0;36m'
clear_colour='\e[0m'

USAGE="./build.sh ../path/to/meteor/app"

info() {
  printf "${cyan}[*]${clear_colour} $@\n"
}

# Quit with exit message
usage() {
  printf "${yellow}[Usage]${clear_colour} ${USAGE}"
  exit 1
}

# check that we have one argument
if [ ${#} = 1 ]; then

    pwd=$(pwd)
    buildDirectory='build'
    buildPath="${pwd}/$buildDirectory"
    bundlePath="${buildPath}/bundle"
    dependencyPath="${bundlePath}/programs/server"

    info "getting the application directory path"
    applicationDirectory=${1}

    info "moving to the application directory"
    pushd ${applicationDirectory}

    info "building the application"
    meteor build --directory ${buildPath}

    info "moving to the dependency directory"
    popd
    pushd ${dependencyPath}

    info "installing dependencies"
    npm install --production

    info "removing fibers and bcrypt installs"
    rm -rf node_modules/fibers
    rm -rf npm/npm-bcrypt

    info "copying the relevant files for beanstalk"
    popd
    cp -R files/ ${bundlePath}

    info "creating deployable archive"
    pushd ${bundlePath}
    zip -r ${pwd}/deploy.zip . .[^.]*
    popd

    info "removing the build directory"
    rm -rf ${buildDirectory}

# no argument supplied, show usage
else
    usage
fi